import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// -- Routing --
import { FeaturesRouting } from './features.routing';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FeaturesRouting
    ]
})
export class FeaturesModule { }
