import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// -- Routing --
import { AuthRouting } from './auth.routing';

// -- Components --
import { LoginComponent } from './login/login.component';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        CommonModule,
        AuthRouting
    ]
})
export class AuthModule { }
