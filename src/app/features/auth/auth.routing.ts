import { RouterModule, Routes } from '@angular/router';

// -- Components --
import { LoginComponent } from './login/login.component';

const AuthRoutes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {
        path: 'login',
        component: LoginComponent
    }
];

export const AuthRouting = RouterModule.forChild(AuthRoutes);
