import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

// -- Components --
import { LayoutComponent } from './layout.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
    declarations: [
        LayoutComponent,
        NavbarComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        LayoutComponent
    ]
})
export class LayoutModule { }
