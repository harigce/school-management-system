import { Component, OnInit, OnDestroy, forwardRef, Inject } from '@angular/core';
import { Subject } from 'rxjs';

// -- Services --
import { NbSidebarService } from '@nebular/theme';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

    userPictureOnly = false;

    // TODO: Need to assign original user details here
    user: any = {
        name: 'Hari Prakash'
    };

    userMenu = [{ title: 'Profile' }, { title: 'Log out' }];

    private destroy$: Subject<void> = new Subject<void>();

    constructor(
        @Inject(forwardRef(() => NbSidebarService)) private sidebarService: NbSidebarService
    ) { }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(true, 'menu-sidebar');

        return false;
    }

}
