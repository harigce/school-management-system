import { Component, OnInit } from '@angular/core';

// -- Configurations --
import { MENU_ITEMS } from '@app/configurations/menu';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    menu = MENU_ITEMS;

    constructor() { }

    ngOnInit() {
    }

}
