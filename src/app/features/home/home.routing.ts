import { RouterModule, Routes } from '@angular/router';

// -- Components --
import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const HomeRoutes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            }
        ]
    }
];

export const HomeRouting = RouterModule.forChild(HomeRoutes);
