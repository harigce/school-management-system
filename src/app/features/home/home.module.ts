import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

// -- Modules --
import { LayoutModule } from '@app/features/layout/layout.module';

// -- Routing --
import { HomeRouting } from './home.routing';

// -- Components --
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home.component';

@NgModule({
    declarations: [
        DashboardComponent,
        HomeComponent
    ],
    imports: [
        LayoutModule,
        SharedModule,
        HomeRouting
    ]
})
export class HomeModule { }
