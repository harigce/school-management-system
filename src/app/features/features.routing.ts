import { RouterModule, Routes } from '@angular/router';

const FeaturesRoutes: Routes = [
    {
        path: '',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'app',
        loadChildren: './home/home.module#HomeModule'

    }
];

export const FeaturesRouting = RouterModule.forChild(FeaturesRoutes);
