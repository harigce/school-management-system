import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// -- Nebular Theme Modules --
import {
    NbMenuModule,
    NbSidebarModule,
    NbLayoutModule,
    NbThemeModule,
    NbActionsModule,
    NbContextMenuModule,
    NbButtonModule,
    NbCardModule,
    NbDialogModule,
    NbIconModule,
    NbUserModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        // -- Nebular theme modules --
        NbThemeModule.forRoot({ name: 'default' }),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbLayoutModule,
        NbActionsModule,
        NbContextMenuModule,
        NbButtonModule,
        NbCardModule,
        NbDialogModule.forRoot(),
        NbIconModule,
        NbEvaIconsModule,
        NbUserModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        // -- Nebular theme modules --
        NbThemeModule,
        NbSidebarModule,
        NbMenuModule,
        NbLayoutModule,
        NbActionsModule,
        NbContextMenuModule,
        NbButtonModule,
        NbCardModule,
        NbDialogModule,
        NbIconModule,
        NbEvaIconsModule,
        NbUserModule
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}

