import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Dashboard',
        icon: 'home-outline',
        link: '/app/dashboard',
    },
    {
        title: 'Users',
        icon: 'people-outline',
        link: '/app/dashboard',
    }
];
