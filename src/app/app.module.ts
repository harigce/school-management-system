import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

// -- Other modules --
import { SharedModule } from './shared/shared.module';

// -- Routing --
import { AppRoutingModule } from './app-routing.module';

// -- Components --
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,

        SharedModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
